
| SUID/SGID Binary | Remove SUID/SGID Bit? | Justification |
|------------------|-----------------------|---------------|
| /usr/bin/chage | Y | Only allows a local user to discover when their password will expire. |
| /usr/bin/crontab | Y | The SUID bit is not necessary on this binary if users are not expected to set up crontab entries. |
| /usr/bin/fusermount | | Rootless Podman containers require non-root users to be able to mount FUSE filesystems. |
| /usr/bin/fusermount3 | | Rootless Podman containers require non-root users to be able to mount FUSE filesystems. |
| /usr/bin/gpasswd | Y | Group passwords are an inherent security risk and should not be used. |
| /usr/bin/ksu | Y | The SUID bit is not necessary on this binary if users will not be using Kerberos to authorize privilege escalation. |
| /usr/bin/newgrp | Y | Users should not need to switch primary group while logged in unless group passwords are being used. |
| /usr/bin/pkexec | | PolicyKit is a dependency of `timedatex`, `realmd`, and `tuned` among other things but can be removed if these packages are not needed. |
| /usr/bin/su | | Necessary for controlled privilege escalation. |
| /usr/bin/sudo | | Necessary for controlled privilege escalation. |
| /usr/lib/polkit-1/polkit-agent-helper-1 | | PolicyKit is a dependency of `timedatex`, `realmd`, and `tuned` among other things but can be removed if these packages are not needed. |
| /usr/libexec/dbus-1/dbus-daemon-launch-helper | | SUID purpose unknown. |
| /usr/libexec/sssd/krb5_child | | SUID purpose unknown. |
| /usr/libexec/sssd/ldap_child | | SUID purpose unknown. |
| /usr/libexec/sssd/proxy_child | | SUID purpose unknown. |
| /usr/libexec/sssd/selinux_child | | SUID purpose unknown. |
| /usr/sbin/grub2-set-bootflag | | Appears to be executed on every login. |
