# RHEL 8 SUID/SGID Binary Audit

Aligned with control 6.1.13 from the RHEL 8 CIS benchmark version 1.0.1

---

## FUSE-related

### /usr/bin/fusermount
* **Status:** `setuid root`
* **Source Package:** `fuse`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/man1/fusermount.1.html

This is required to allow the mounting of FUSE filesystems. In particular this is a dependency of `podman` or `sshfs`.<br>In Podman, this is used as part of `fuse-overlayfs` when unprivileged users need to reassemble the layers from a container during the `pull` process:
> In rootless Podman, we actually use a fuse-overlayfs executable to create the layer. Rootfull uses the kernel’s overlayfs driver. Currently, the kernel does not allow rootless users to mount overlay filesystems, but they can mount FUSE filesystems. -- from [Red Hat](https://www.redhat.com/sysadmin/behind-scenes-podman)

The SUID bit could be removed from this binary if there is no use-case on the system for unprivileged users to mount/unmount FUSE filesystems.

### /usr/bin/fusermount3
* **Status:** `setuid root`
* **Source Package:** `fuse3`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/man1/fusermount3.1.html

This is the same as `/usr/bin/fusermount`, but for FUSE v3 filesystems.

The SUID bit could be removed from this binary if there is no use-case on the system for unprivileged users to mount/unmount FUSE filesystems.

---

## shadow-utils

### /usr/bin/chage
* **Status:** `setuid root`
* **Source Package:** `shadow-utils`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/en/man1/chage.1.html

The `chage` utility deals with password age and, as such, requires SUID to access the `/etc/shadow` file when invoked by a non-root user.

> The chage command is restricted to the root user, except for the -l option, which may be used by an unprivileged user to determine when his/her password or account is due to expire. -- from [Canonical](https://manpages.ubuntu.com/manpages/bionic/en/man1/chage.1.html)

The SUID bit could be removed from this binary if there is no intent to allow users to audit when their passwords will expire.

### /usr/bin/gpasswd
* **Status:** `setuid root`
* **Source Package:** `shadow-utils`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/en/man1/gpasswd.1.html

The `gpasswd` utility deals with administering `/etc/group` and `/etc/gshadow` which can be used to set group passwords.

This binary requires SUID to access the `/etc/gshadow` file when invoked by a non-root user.

The SUID bit could be removed from this binary if group passwords are not intended to be used.

### /usr/bin/newgrp
* **Status:** `setuid root`
* **Source Package:** `shadow-utils`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/en/man1/newgrp.1.html

> The newgrp command is used to change the current group ID during a login session. If the optional `-` flag is given, the user's environment will be reinitialized as though the user had logged in, otherwise the current environment, including current working directory, remains unchanged.

This binary requires SUID to access the `/etc/gshadow` file when invoked by a non-root user.

The SUID bit could be removed from this binary if users are not expected to need to switch to a different primary group while logged in.

---

## cron

### /usr/bin/crontab
* **Status:** `setuid root`
* **Source Package:** `cronie`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/en/man1/crontab.1.html

The `crontab` utility allows users to edit or list their own crontab file.

This binary requires SUID to access the generated crontabs, which are stored within `/var/spool/cron/` and owned by `root:root`.

The SUID bit could be removed from this binary if it is not intended that non-root users will be configuring their own `crontab` files.


---

## Active Directory

### /usr/bin/ksu
* **Status:** `setuid root`
* **Source Package:** `krb5-workstation`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/en/man1/ksu.1.html

`ksu` is the "Kerberized Super-User" binary, which implements a Kerberized version of the `su` program. Effectively, it can be used in place of `sudo`, where authorization for the elevation is provided by Kerberos and not by the local `/etc/sudoers` file.

The SUID bit could be removed from this binary if it is not intended that users elevate privileges by authenticating to Kerberos - e.g. if `sudo` is used instead.

### /usr/libexec/sssd/krb5_child
* **Status:** `setuid root`
* **Source Package:** `sssd-krb5-common`
* **Docs:** _N/A_

WIP - Documentation is sparse on this particular binary and its function.

### /usr/libexec/sssd/ldap_child
* **Status:** `setuid root`
* **Source Package:** `sssd-krb5-common`
* **Docs:** _N/A_

WIP - Documentation is sparse on this particular binary and its function.


### /usr/libexec/sssd/proxy_child
* **Status:** `setuid root`
* **Source Package:** `sssd-proxy`
* **Docs:** _N/A_

WIP - Documentation is sparse on this particular binary and its function.


### /usr/libexec/sssd/selinux_child
* **Status:** `setuid root`
* **Source Package:** `sssd-ipa`
* **Docs:** _N/A_

WIP - Documentation is sparse on this particular binary and its function.

---

## PolicyKit

### /usr/bin/pkexec
* **Status:** `setuid root`
* **Source Package:** `polkit`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/en/man1/pkexec.1.html

This binary is part of PolicyKit and is used to execute programs as another user. Without specifying a username, `pkexec` will attempt to launch a program as `root`.

As of August 2021, `polkit` is a dependency of the packages listed below. Packages highlighted in **bold** are included in a standard RHEL 8 "Minimal" install:
- accountsservice
- gnome-initial-setup
- gnome-shell
- libvirt-daemon
- libvirt-dbus
- lshw-gui
- pcsc-lite
- polkit-pkla-compat
- realmd
- rtkit
- scap-workbench
- setroubleshoot-server
- sssd-polkit-rules
- **timedatex**
- **tuned**
- tuned-gtk
- usbguard-dbus
- xorg-x11-drv-intel

The `timedatex` package is useful for dealing with the system timezone and for setting the RTC, and the `tuned` package is a system performance daemon.

It is also worth noting that, as seen above, the `realmd` package - necessary for authenticating to Active Directory from the RHEL system - depends on PolicyKit.

PolicyKit should likely be left alone for the reasons outlined above. Suggested remediation would be complete removal of `polkit` if it can be confirmed that none of the packages above (or any other that depends on `polkit`) are necessary.

### /usr/lib/polkit-1/polkit-agent-helper-1
* **Status:** `setuid root`
* **Source Package:** `polkit`
* **Docs:** _N/A_

Documentation is sparse on this particular binary and its function but it is clear that it is part of `polkit` and, as such, likely performs functions that can be considered a dependency of the packages listed in the review of `/usr/bin/pkexec` above.

---

## systemd

### /usr/libexec/dbus-1/dbus-daemon-launch-helper
* **Status:** `setuid root`
* **Source Package:** `dbus-daemon`
* **Docs:** _N/A_

WIP - Documentation is sparse on this binary and its function. The `dbus-daemon` package is a dependency of `systemd` so it cannot be removed.

---

## GRUB

### /usr/sbin/grub2-set-bootflag
* **Status:** `setuid root`
* **Source Package:** `grub2-tools-minimal`
* **Docs:** _N/A_

> grub-set-bootflag is a command line [utility] to set bootflags in GRUB's stored environment. -- [Red Hat](https://bugzilla.redhat.com/show_bug.cgi?id=1764925#c7)

More information is available about the purpose of this utility here in the [original commit](https://github.com/rhboot/grub2/commit/9a1d81c028d88c548181521619f954bc97714d88) adding it to the GRUB2 project.

This utility allows a non-root user to update the `grubenv` file (at `/boot/grub2/grubenv`) to reflect things like a successful system boot, or to request to show the GRUB menu once on next boot.

I'm unclear what the overall impact of removing the SUID bit on this would be, but it's likely to be low. It is worth noting that vulnerabilities have been found in this code in the past, including by Tavis Omandy, who found that:

> When the utility is run under resource pressure (by setting RLIMIT), it can cause grub2 config files to be truncated leaving the system non-bootable on subsequent reboots. -- [Red Hat](https://bugzilla.redhat.com/show_bug.cgi?id=1764925#c0)

Auditing of this command in a production environment (using the `privileged` auditd rule on a system configured to the CIS benchmark) suggests that it is triggered upon every user login.

The SUID bit should not be removed from this binary.

---

## Privilege Escalation

### /usr/bin/sudo
* **Status:** `setuid root`
* **Source Package:** `sudo`
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/en/man8/sudo.8.html

The `sudo` utility is the most commonly supported privilege escalation tool on Linux distributions. It is the official tool supported by Red Hat and is included in the RHEL minimal packageset.

Alternatives with smaller codebases exist, such as `doas`, but `sudo` is the Red Hat supported approach.

The SUID bit should not be removed from this binary.

### /usr/bin/su
* **Status:** `setuid root`
* **Source Package:** util-linux
* **Docs:** https://manpages.ubuntu.com/manpages/bionic/en/man1/su.1.html

The `su` utility is another utility for privilege escalation on Linux distributions. It is also shipped as part of RHEL.

Using the `su` utility, a user can "become" another user on the system as if the user had logged in directly.

The `su` utility passes the current environment to the newly-spawned session and resets the `$PATH` to (for `root`):
```
/sbin:/bin:/usr/sbin:/usr/bin
```

Unlike `sudo`, the `su` utility does not source the same variables for the user session (as with the `$PATH` above, and with variables such as the ones held in `/etc/profile`). Using `sudo -i` is more true to an actual user login than `su`.

For the reasons above, `sudo -i` should generally be preferred over `su` for starting interactive user sessions on a system.

Although `sudo` is generally a preferable method of privilege escalation to `su`, the `su` binary requires the SUID bit to perform its general function.

The SUID bit should not be removed from this binary.
