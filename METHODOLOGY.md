Find SUID binaries:
```
sudo find / -xdev -type f -perm /4000 -exec ls -lah {} \;
```

Find SGID binaries:
```
sudo find / -xdev -type f -perm /2000 -exec ls -lah {} \;
```

Find SUID or SGID binaries:
```
sudo find / -xdev -type f -perm /6000 -exec ls -lah {} \;
```

Searching for a package which provides a file (`repoquery` requires the `yum-utils` package to be installed):
```
sudo repoquery -f /usr/bin/fusermount
```
